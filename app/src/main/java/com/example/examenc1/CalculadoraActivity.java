    package com.example.examenc1;

    import android.os.Bundle;
    import android.view.View;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.TextView;
    import android.widget.Toast;

    import androidx.appcompat.app.AppCompatActivity;

    public class CalculadoraActivity extends AppCompatActivity {
            private EditText txtNum1, txtNum2;
            private TextView lblResultado;
            private Button btnSuma, btnResta, btnMult, btnDiv, btnLimpiar, btnRegresar;
            private Calculadora calculadora;

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_calculadora);

                txtNum1 = findViewById(R.id.txtNum1);
                txtNum2 = findViewById(R.id.txtNum2);
                lblResultado = findViewById(R.id.lblResultado);
                btnSuma = findViewById(R.id.btnSuma);
                btnResta = findViewById(R.id.btnResta);
                btnMult = findViewById(R.id.btnMult);
                btnDiv = findViewById(R.id.btnDiv);
                btnLimpiar = findViewById(R.id.btnLimpiar);
                btnRegresar = findViewById(R.id.btnRegresar);
                calculadora = new Calculadora();


                btnSuma.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (setValuesFromInput()) {
                            lblResultado.setText("Resultado: " + calculadora.suma());
                        }
                    }
                });

                btnResta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (setValuesFromInput()) {
                            lblResultado.setText("Resultado: " + calculadora.resta());
                        }
                    }
                });

                btnMult.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (setValuesFromInput()) {
                            lblResultado.setText("Resultado: " + calculadora.multiplicacion());
                        }
                    }
                });

                btnDiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (setValuesFromInput()) {
                            try {
                                lblResultado.setText("Resultado: " + calculadora.division());
                            } catch (ArithmeticException e) {
                                Toast.makeText(CalculadoraActivity.this, "División por cero no permitida", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

                btnLimpiar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        limpiarCampos();
                    }
                });

                btnRegresar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            }

            private boolean setValuesFromInput() {
                try {
                    float num1 = Float.parseFloat(txtNum1.getText().toString());
                    float num2 = Float.parseFloat(txtNum2.getText().toString());
                    calculadora.setNum1(num1);
                    calculadora.setNum2(num2);
                    return true;
                } catch (NumberFormatException e) {
                    Toast.makeText(this, "Por favor, ingrese números válidos.", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }

            private void limpiarCampos() {
                txtNum1.setText("");
                txtNum2.setText("");
                lblResultado.setText("Resultado:");
            }

        }
